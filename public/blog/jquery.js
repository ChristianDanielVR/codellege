$(document).ready(function(){
var altura = $('.menu').offset().top;

$(window).on('scroll', function(){
 if ( $(window).scrollTop() > altura ){
  $('.menu').addClass('menu-fixed');
     }
     else {
        $('.menu').removeClass('menu-fixed');
        }
    });
    $('a.tab').click(function(event){
      event.preventDefault()
      $('.tab-content.show').removeClass('show')
      $('.tab.active').removeClass('active')
      $(this).addClass('active')
      var id = $(this).attr('href')
      $(id).addClass('show')
    })
    $('.open').click(function () {
    	$('.mod, .overla').addClass('active')

    })
    $('.close1').click(function (){
    	$('.mod, .overla').removeClass('active')
    })
    $(document).bind('keydown', function(event){
    	if (event.which == 27){
    		$('.mod, .overla').removeClass('active')
    	}
    })
    $('.open2').click(function () {
      $('.mod2, .overla2').addClass('active')

    })
    $('.close2').click(function (){
      $('.mod2, .overla2').removeClass('active')
    })
    $(document).bind('keydown', function(event){
      if (event.which == 27){
        $('.mod2, .overla2').removeClass('active')
      }
    })
    $('.open3').click(function () {
      $('.mod3, .overla3').addClass('active')

    })
    $('.close3').click(function (){
      $('.mod3, .overla3').removeClass('active')
    })
    $(document).bind('keydown', function(event){
      if (event.which == 27){
        $('.mod3, .overla3').removeClass('active')
      }
    })
    $('.open4').click(function () {
      $('.mod4, .overla4').addClass('active')

    })
    $('.close4').click(function (){
      $('.mod4, .overla4').removeClass('active')
    })
    $(document).bind('keydown', function(event){
      if (event.which == 27){
        $('.mod4, .overla4').removeClass('active')
      }
    })
    $(".slider > div:gt(0)").hide();

    setInterval(function(){
      $(".slider > div:first-child")
          .fadeOut(1000)
          .next()
          .fadeIn(1000)
          .end()
          .appendTo(".slider");
    }, 3000);

});
